<?php
require('./system.php');

loadTool('ajax.php');

if (	$config['p_logic'] != 'usual' 
	and $config['p_logic'] != 'xauth'
	and $config['p_logic'] != 'authme') aExit(1,'Registration is blocked. Used auth script from main CMS');

BDConnect('register');

loadTool('wocapi.class.php');
loadTool('user.class.php');
	$api = new WoCAPI();
RefreshBans();
	if(MCR_LANG=='ru_RU') $REG_CONFIRM_INFO = 'Сообщение с данными для активации аккаунта в сети WorldsOfCubes.NET отправлено на Ваш почтовый адрес. Пока Вы не активируете аккаунт, Вы не сможете зайти.';
		else $REG_CONFIRM_INFO = 'Your activation details for account in WorldsOfCubes Network are sent at your e-mail';
	
$login  = $_POST['login'];
$pass   = $_POST['pass'];
$repass = $_POST['repass'];	
	
$female = (!(int)$_POST['female'])? 0 : 1;
$email  = filter_var($_POST['email'], FILTER_VALIDATE_EMAIL); 
	
	
if (empty($login) || empty($pass) || empty($repass) || empty($_POST['email'])) aExit(1, lng('INCOMPLETE_FORM'));
    
	if (!preg_match("/^[a-zA-Z0-9_-]+$/", $login))  $rcodes[] = 2; 
    if (!preg_match("/^[a-zA-Z0-9_-]+$/", $pass))   $rcodes[] = 3;
    if (!preg_match("/^[a-zA-Z0-9_-]+$/", $repass)) $rcodes[] = 4;
	if (!$email)                                    $rcodes[] = 12;

	if ((strlen($login) < 4)  or (strlen($login) > 15))  $rcodes[] = 6;
	if ((strlen($pass) < 4)   or (strlen($pass) > 15))   $rcodes[] = 7;
	if ((strlen($repass) < 4) or (strlen($repass) > 15)) $rcodes[] = 8;
    if (strlen($email) > 50)   							 $rcodes[] = 13;
	if (strcmp($pass,$repass)) 							 $rcodes[] = 9;
	
	if(!$api->register($login, $pass, $repass, $female, $email, GetRealIp()))
		switch ($api->error) {
			case 1: aExit($api->error, lng('INCORRECT').'. ('.lng('LOGIN').')'); break;
			case 2: aExit($api->error, lng('INCORRECT').'. ('.lng('PASS').')'); break;
			case 3: aExit($api->error, lng('INCORRECT').'. ('.lng('REPASS').')'); break;
			case 4: aExit($api->error, lng('INCORRECT').'. ('.lng('EMAILL').')'); break;
			
			case 5: aExit($api->error, lng('INCORRECT_LEN').'. ('.lng('LOGIN').')'); break;
			case 6: aExit($api->error, lng('INCORRECT_LEN').'. ('.lng('PASS').')');
			case 7: aExit($api->error, lng('INCORRECT_LEN').'. ('.lng('REPASS').')'); break;
			case 8: aExit($api->error, lng('REPASSVSPASS')); break;
			case 9: aExit($api->error, lng('INCORRECT_LEN').'. ('.lng('EMAILL').')'); break;
			case 10: aExit($api->error, lng('IP_BANNED')); break;
			
			case 12: aExit($api->error, lng('AUTH_EXIST_LOGIN')); break;
			case 13: aExit($api->error, lng('AUTH_EXIST_EMAIL')); break;
			default : aExit($api->error, $api->error); break;
		}
	
	aExit(0, lng('REG_COMPLETE') .'. '. $REG_CONFIRM_INFO);
	unset($tmp_user);
?>
