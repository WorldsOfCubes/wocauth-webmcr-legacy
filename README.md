# WoCAuth v1.0 and v2.0 scripts

 - **Author** : WorldsOfCubes Group
 - **Version** : 2.0
 - **Website** : [WorldsOfCubes.NET](http://WorldsOfCubes.NET)

## License 

 [WTFPL](http://wtfpl.net/about/) 
 
## Information

 Client-side authentification scripts used in 2012-2013. Now open source. Compable with webMCR/webMCRex. Not recommended to use.

### Installation

 - Put into folder with webMCR