<?php
        class WoCAPI {
             
                public $mcUsername = '';
                public $email = '';
                public $error = -1;
                public $gender = '';
             
                public function register($login, $pass, $repass, $female, $email, $ip){
                        $url = 'http://worldsofcubes.net/api2/register.php';
                        $params = array(
                                "login" => $login,
                                "pass" => $pass,
                                "repass" => $repass,
                                "female" => $female,
                                "email" => $email,
                                "ip" => $ip,
                        );
                        $mcSocket = curl_init();
                        curl_setopt_array($mcSocket, array(
                                CURLOPT_URL => $url,
                                CURLOPT_RETURNTRANSFER => true,
                                CURLOPT_POST => true,
                                CURLOPT_POSTFIELDS => http_build_query($params)
                        ));
                        $mcOutput = curl_exec($mcSocket);
                        curl_close($mcSocket);
                        $this->error = (int)$mcOutput;
                        // ��������� ����� ����� WorldsOfCubes.NET
                        if((int)$mcOutput == 0){
                                return true;
                        }else{
                                return false;
                        }
                }
				
                public function login($username, $password){
                        $url = 'http://worldsofcubes.net/api2/auth.php';
                        $params = array(
                                "user" => $username,
                                "password" => $password,
                        );
                        $mcSocket = curl_init();
                        curl_setopt_array($mcSocket, array(
                                CURLOPT_URL => $url,
                                CURLOPT_RETURNTRANSFER => true,
                                CURLOPT_POST => true,
                                CURLOPT_POSTFIELDS => http_build_query($params)
                        ));
                        $mcOutput = curl_exec($mcSocket);
                        curl_close($mcSocket);
                        $this->error = (int)$mcOutput;
                        // ��������� ����� ����� WorldsOfCubes.NET
                        if (($this->error>0) and ($this->error<=3)) return false;
                        $mcValues = explode(':', $mcOutput);
                        if(count($mcValues) > 3){
                                // ����� ������ ��� ������ �� ������ ����� WorldsOfCubes.NET
                                $this->mcUsername = $mcValues[2]; //��� ������
                                $this->email = $mcValues[3]; //����
                                $this->gender = $mcValues[1]; //���
                                return true;
                        }else{
                                return false;
                             
                        }
                }
     
        }    

?>